#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#define CL_TARGET_OPENCL_VERSION 120
#include "ocl_boiler.h"

size_t gws_align_init;
size_t gws_align_reinit;
size_t gws_align_chi;
cl_event matinit(cl_kernel matinit_k, cl_command_queue que, cl_mem m_A, cl_int num_righe, cl_int num_cols, cl_int _lws) {
	const int gws[] = { round_mul_up(num_cols, gws_align_init), round_mul_up(num_righe, gws_align_init) };
	const int lws[] = { _lws, _lws };
	cl_int err;
	cl_event matinit_evt;
	cl_uint i = 0;
	err = clSetKernelArg(matinit_k, i++, sizeof(m_A), &m_A);
	ocl_check(err, "set parameter matinit failed", i - 1);
	err = clSetKernelArg(matinit_k, i++, sizeof(num_righe), &num_righe);
	ocl_check(err, "set parameter matinit failed", i - 1);
	err = clSetKernelArg(matinit_k, i++, sizeof(num_cols), &num_cols);
	ocl_check(err, "set parameter matinit failed", i - 1);

	err = clEnqueueNDRangeKernel(que, matinit_k, 2, NULL, gws, lws, 0, NULL, &matinit_evt);
	ocl_check(err, "enqueue matinit failed");
	return matinit_evt;
}

cl_event computesums(cl_kernel computesums_k, cl_command_queue que, cl_mem m_A, cl_mem sum_righe,
	cl_mem sum_cols, cl_mem sum_tot, cl_int num_righe, cl_int num_cols, cl_int _lws, cl_event wait) {
	const int gws[] = { round_mul_up(num_cols, gws_align_init), round_mul_up(num_righe, gws_align_init) };
	const int lws[] = { _lws, _lws };
	cl_int err;
	cl_event computesums_evt;
	cl_uint i = 0;
	err = clSetKernelArg(computesums_k, i++, sizeof(m_A), &m_A);
	ocl_check(err, "set parameter computesums failed", i - 1);
	err = clSetKernelArg(computesums_k, i++, sizeof(sum_righe), &sum_righe);
	ocl_check(err, "set parameter computesums failed", i - 1);
	err = clSetKernelArg(computesums_k, i++, lws[0] * sizeof(cl_int), NULL);
	ocl_check(err, "set parameter computesums failed", i - 1);
	err = clSetKernelArg(computesums_k, i++, sizeof(sum_cols), &sum_cols);
	ocl_check(err, "set parameter computesums failed", i - 1);
	err = clSetKernelArg(computesums_k, i++, lws[0] * sizeof(cl_int), NULL);
	ocl_check(err, "set parameter computesums failed", i - 1);
	err = clSetKernelArg(computesums_k, i++, sizeof(sum_tot), &sum_tot);
	ocl_check(err, "set parameter computesums failed", i - 1);
	err = clSetKernelArg(computesums_k, i++, lws[0] * sizeof(cl_int), NULL);
	ocl_check(err, "set parameter computesums failed", i - 1);
	err = clSetKernelArg(computesums_k, i++, sizeof(num_righe), &num_righe);
	ocl_check(err, "set parameter computesums failed", i - 1);
	err = clSetKernelArg(computesums_k, i++, sizeof(num_cols), &num_cols);
	ocl_check(err, "set parameter computesums failed", i - 1);
	err = clSetKernelArg(computesums_k, i++, sizeof(lws[0]), &lws[0]);
	ocl_check(err, "set parameter computesums failed", i - 1);

	err = clEnqueueNDRangeKernel(que, computesums_k, 2, NULL, gws, lws, 0, NULL, &computesums_evt);
	ocl_check(err, "enqueue computesums failed");
	return computesums_evt;
}

cl_event reinit(cl_kernel reinit_k, cl_command_queue que, cl_mem m_attesi, cl_mem m_A, cl_mem sum_righe, cl_mem sum_cols,
	cl_mem sum_tot, cl_int num_righe, cl_int num_cols, cl_event wait) {
	const int gws[] = { round_mul_up(num_cols, gws_align_reinit), round_mul_up(num_righe, gws_align_reinit) };
	cl_int err;
	cl_event reinit_evt;
	cl_uint i = 0;
	err = clSetKernelArg(reinit_k, i++, sizeof(m_attesi), &m_attesi);
	ocl_check(err, "set parameter reinit failed", i - 1);
	err = clSetKernelArg(reinit_k, i++, sizeof(m_A), &m_A);
	ocl_check(err, "set parameter reinit failed", i - 1);
	err = clSetKernelArg(reinit_k, i++, sizeof(sum_righe), &sum_righe);
	ocl_check(err, "set parameter reinit failed", i - 1);
	err = clSetKernelArg(reinit_k, i++, sizeof(sum_cols), &sum_cols);
	ocl_check(err, "set parameter reinit failed", i - 1);
	err = clSetKernelArg(reinit_k, i++, sizeof(sum_tot), &sum_tot);
	ocl_check(err, "set parameter reinit failed", i - 1);
	err = clSetKernelArg(reinit_k, i++, sizeof(num_righe), &num_righe);
	ocl_check(err, "set parameter reinit failed", i - 1);
	err = clSetKernelArg(reinit_k, i++, sizeof(num_cols), &num_cols);
	ocl_check(err, "set parameter reinit failed", i - 1);

	err = clEnqueueNDRangeKernel(que, reinit_k, 2, NULL, gws, NULL, 1, &wait, &reinit_evt);
	ocl_check(err, "enqueue reinit failed");
	return reinit_evt;
}

cl_event reduce_chi(cl_kernel reduce_chi_k, cl_command_queue que, cl_mem m_attesi,
	cl_mem chi_accum, cl_int nels, cl_int _lws, cl_int _gws, const cl_event wait) {
	const cl_int gws[] = { _gws * _lws };
	const size_t lws[] = { _lws };
	cl_int err;
	cl_event reduce_chi_evt;
	cl_uint i = 0;
	err = clSetKernelArg(reduce_chi_k, i++, sizeof(m_attesi), &m_attesi);
	ocl_check(err, "set parameter reduce_chi failed", i - 1);
	err = clSetKernelArg(reduce_chi_k, i++, sizeof(chi_accum), &chi_accum);
	ocl_check(err, "set parameter reduce_chi failed", i - 1);
	err = clSetKernelArg(reduce_chi_k, i++, sizeof(cl_float8) * _lws, NULL);
	ocl_check(err, "set parameter reduce_chi failed", i - 1);
	err = clSetKernelArg(reduce_chi_k, i++, sizeof(lws[0]), &lws[0]);
	ocl_check(err, "set parameter reduce_chi failed", i - 1);
	err = clSetKernelArg(reduce_chi_k, i++, sizeof(nels), &nels);
	ocl_check(err, "set parameter reduce_chi failed", i - 1);

	err = clEnqueueNDRangeKernel(que, reduce_chi_k, 1, NULL, gws, lws, 1, &wait, &reduce_chi_evt);
	ocl_check(err, "enqueue reduce_chi failed");
	return reduce_chi_evt;
}

/*Inizializza buffer host*/
void h_init(int* h_A, int* h_sum_righe, int* h_sum_cols, int num_righe, int num_cols) {
	for (int i = 0; i < num_righe; i++)
		h_sum_righe[i] = 0;

	for (int i = 0; i < num_cols; i++)
		h_sum_cols[i] = 0;

	for (int i = 0; i < num_righe; i++) {
		for (int j = 0; j < num_cols; j++) {
			h_A[i * num_cols + j] = i + j;
		}
	}
}

/*Calcola somme su host*/
void h_sum(int* h_A, int* h_sum_righe, int* h_sum_cols, int num_righe, int num_cols, int* h_sum_tot) {
	for (int i = 0; i < num_righe; i++) {
		for (int j = 0; j < num_cols; j++) {
			h_sum_righe[i] += h_A[i * num_cols + j];
			h_sum_cols[j] += h_A[i * num_cols + j];
			*h_sum_tot += h_A[i * num_cols + j];
		}
	}
}

void h_reinit(float* h_attesi, int* m_A, int* map_righe, int* map_cols,
	int map_tot, int num_righe, int num_cols) {
	for (int i = 0; i < num_righe; i++) {
		for (int j = 0; j < num_cols; j++) {
			float value = ((_int64)map_righe[i] * (_int64)map_cols[j]) / (float)map_tot;
			h_attesi[i * num_cols + j] = ((m_A[i * num_cols + j] - value) * (m_A[i * num_cols + j] - value)) / (float)value;
		}
	}
}

void h_reduce_chi(float* m_attesi, int num_righe, int num_cols, float* chi_accum) {
	for (int i = 0; i < num_righe; i++) {
		for (int j = 0; j < num_cols; j++) {
			*chi_accum += m_attesi[i * num_cols + j];
		}
	}
}

void main(int argc, char* argv[]) {
	/*Controlla se il numero di parametri � corretto: sono necessari numeri di righe e colonne*/
	if (argc < 6) {
		fprintf(stderr, "inserisci numero di righe e colonne, le dimensioni del lws e la dimensione del gws/lws della riduzione\n");
		exit(1);
	}


	const int num_righe = atoi(argv[1]); //crea variabile che contiene numero righe
	const int num_cols = atoi(argv[2]); //crea variabile che contiene numero colonne
	const int _lws = atoi(argv[3]);
	const int _reduxlws = atoi(argv[4]);
	const int gws = atoi(argv[5]);
	const int memsize = num_righe * num_cols * sizeof(int); //crea variabile che rappresenta la memoria occupata dalla matrice principale

	/*Boilerplate OCL*/
	cl_platform_id p = select_platform();
	cl_device_id d = select_device(p);
	cl_context ctx = create_context(p, d);
	cl_command_queue que = create_queue(ctx, d);
	cl_program prog = create_program("ChiQuadroReduxLmem.ocl", ctx, d);

	cl_int err;

	/*Inizializza i kernel del codice device*/
	cl_kernel matinit_k = clCreateKernel(prog, "matinit", &err);
	ocl_check(err, "errore nella creazione del kernel matinit");

	cl_kernel computesums_k = clCreateKernel(prog, "computesums", &err);
	ocl_check(err, "errore nella creazione del kernel computesums");

	cl_kernel reinit_k = clCreateKernel(prog, "reinit", &err);
	ocl_check(err, "errore nella creazione del kernel reinit");

	cl_kernel reduce_chi_k = clCreateKernel(prog, "reduce_chi_lmem", &err);
	ocl_check(err, "errore nella creazione del kernel reduce_chi");

	/*Query sul device per ottenere i preferred work group size multiple di ogni kernel*/
	err = clGetKernelWorkGroupInfo(matinit_k, d, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(gws_align_init), &gws_align_init, NULL);
	ocl_check(err, "errore nel calcolo del preferred work group size multiple");
	err = clGetKernelWorkGroupInfo(computesums_k, d, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(gws_align_init), &gws_align_init, NULL);
	ocl_check(err, "errore nel calcolo del preferred work group size multiple");
	err = clGetKernelWorkGroupInfo(reinit_k, d, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(gws_align_reinit), &gws_align_reinit, NULL);
	ocl_check(err, "errore nel calcolo del preferred work group size multiple");
	err = clGetKernelWorkGroupInfo(reduce_chi_k, d, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(gws_align_chi), &gws_align_chi, NULL);
	ocl_check(err, "errore nel calcolo del preferred work group size multiple");

	/*Crea buffer device per la computazione e buffer host per la verifica seriale*/
	cl_mem m_A = NULL, sum_righe = NULL, sum_cols = NULL, sum_tot = NULL,
		m_attesi = NULL, chi_accum = NULL;
	int* h_A = NULL, * h_sum_righe = NULL, * h_sum_cols = NULL;
	float* h_attesi = NULL;
	int h_sum_tot = 0;
	float h_chi_accum = 0, chi_quadro;

	/*Inizializza buffer device*/
	m_A = clCreateBuffer(ctx, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY, num_righe*num_cols*sizeof(cl_int), NULL, &err);
	ocl_check(err, "errore nella creazione del buffer m_A");
	sum_righe = clCreateBuffer(ctx, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY, num_righe * sizeof(cl_int), NULL, &err);
	ocl_check(err, "errore nella creazione del buffer sum_righe");
	sum_cols = clCreateBuffer(ctx, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY, num_cols * sizeof(cl_int), NULL, &err);
	ocl_check(err, "errore nella creazione del buffer sum_cols");
	sum_tot = clCreateBuffer(ctx, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY, sizeof(cl_int), NULL, &err);
	ocl_check(err, "errore nella creazione del buffer sum_tot");

	/*Inizializza buffer host*/
	h_A = (int*)malloc(memsize);
	h_sum_righe = (int*)malloc(sizeof(int) * num_righe);
	h_sum_cols = (int*)malloc(sizeof(int) * num_cols);

	/*Calcola Chi su host*/
	h_init(h_A, h_sum_righe, h_sum_cols, num_righe, num_cols);

	h_sum(h_A, h_sum_righe, h_sum_cols, num_righe, num_cols, &h_sum_tot);

	h_attesi = (float*)malloc(num_righe * num_cols * sizeof(float));

	h_reinit(h_attesi, h_A, h_sum_righe, h_sum_cols, h_sum_tot, num_righe, num_cols);

	h_reduce_chi(h_attesi, num_righe, num_cols, &h_chi_accum);

	/*Crea eventi OpenCL*/
	cl_event matinit_evt, computesums_evt, reinit_evt, read_evt;

	/*Inizializza matrice host e device*/
	matinit_evt = matinit(matinit_k, que, m_A, num_righe, num_cols, _lws);

	computesums_evt = computesums(computesums_k, que, m_A, sum_righe, sum_cols, sum_tot, num_righe, num_cols, _lws, matinit_evt);
	/*Crea buffer valori aspettati, device ed host*/
	m_attesi = clCreateBuffer(ctx, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY, num_righe * num_cols * sizeof(float), NULL, &err);
	ocl_check(err, "errore nella creazione del buffer m_attesi");

	/*Inizializza buffer con i valori aspettati*/
	reinit_evt = reinit(reinit_k, que, m_attesi, m_A, sum_righe, sum_cols, sum_tot, num_righe, num_cols, computesums_evt);

	/*Verifica che l'operazione sia andata a buon fine*/
	chi_accum = clCreateBuffer(ctx, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY, gws * sizeof(float), NULL, &err);
	ocl_check(err, "errore nella creazione del buffer chi_accum");

	const int nels = num_righe * num_cols;
	cl_event* reduce_evts = (cl_event*)malloc(sizeof(cl_event) * 3);
	reduce_evts[0] = reinit_evt;
	reduce_evts[1] = reduce_chi(reduce_chi_k, que, m_attesi, chi_accum, nels / 8, _reduxlws, gws, reduce_evts[0]);
	reduce_evts[2] = reduce_chi(reduce_chi_k, que, chi_accum, m_attesi, gws / 8, _reduxlws, 1, reduce_evts[1]);

	clEnqueueReadBuffer(que, m_attesi, CL_TRUE, 0, sizeof(chi_quadro), &chi_quadro, 1, &reduce_evts[2], &read_evt);

	printf("Chi � %f\n", chi_quadro);
	printf("Chi � %f\n", h_chi_accum);

	/*Calcola e stampa banda passante e runtimes*/

	int tmpRighe = num_righe;
	int tmpCols = num_cols;
	const double runtime_init = runtime_ms(matinit_evt);
	const double runtime_sum = runtime_ms(computesums_evt);
	const double runtime_reinit = runtime_ms(reinit_evt);
	const double runtime_chi_1 = runtime_ms(reduce_evts[1]);
	const double runtime_chi_2 = runtime_ms(reduce_evts[2]);
	const double runtime_chi_tot = runtime_chi_1 + runtime_chi_2;

	const double init_bw = (1.0 * memsize) / 1.0e6 / runtime_init;
	const double sum_bw = ((1.0 * memsize) + (_lws * sizeof(int) * 2) + sizeof(int)) / 1.0e6 / runtime_sum;
	const double reinit_bw = ((num_righe * num_cols) * (4.0 * sizeof(int) + sizeof(float))) / 1.0e6 / runtime_reinit;
	const double reduce1_bw = ((num_righe * num_cols * sizeof(float)) + (gws * sizeof(float))) / 1.0e6 / runtime_chi_1;
	const double reduce2_bw = ((gws * sizeof(float)) + (sizeof(float))) / 1.0e6 / runtime_chi_2;

	printf("init: %dx%d els in %g ms %g GB/s %g GE/s\n",
		num_righe, num_cols, runtime_init, init_bw, num_righe * num_cols / 1.0e6 / runtime_init);
	printf("sum: %dx%d els in %g ms %g GB/s %g GE/s\n",
		num_righe, num_cols, runtime_sum, sum_bw, num_righe * num_cols / 1.0e6 / runtime_sum);
	printf("reinit: %dx%d els in %g ms %g GB/s %g GE/s\n",
		num_righe, num_cols, runtime_reinit, reinit_bw, num_righe * num_cols / 1.0e6 / runtime_reinit);
	printf("reduce1: %d els in %g ms %g GB/s %g GE/s\n",
		num_righe * num_cols, runtime_chi_1, reduce1_bw, num_righe * num_cols / 1.0e6 / runtime_reinit);
	printf("reduce2: %d els in %g ms %g GB/s %g GE/s\n",
		gws, runtime_chi_2, reduce2_bw, gws / 1.0e6 / runtime_reinit);

	printf("Tempo complessivo di riduzione: %lf msec\n", runtime_chi_tot);
	printf("Runtime complessivo: %g ms\n", runtime_init + runtime_reinit + runtime_sum + runtime_chi_tot);

	free(h_A);
	free(h_sum_righe);
	free(h_sum_cols);
	free(h_attesi);
	clReleaseKernel(matinit_k);
	clReleaseKernel(reinit_k);
	clReleaseKernel(reduce_chi_k);
	clReleaseProgram(prog);
	clReleaseCommandQueue(que);
	clReleaseContext(ctx);
	clReleaseMemObject(m_A);
	clReleaseMemObject(sum_righe);
	clReleaseMemObject(sum_cols);
	clReleaseMemObject(sum_tot);
	clReleaseMemObject(m_attesi);
	clReleaseMemObject(chi_accum);
	exit(0);
}
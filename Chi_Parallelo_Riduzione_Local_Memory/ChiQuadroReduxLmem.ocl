kernel void matinit(global int* restrict m_A, const int righe, const int colonne){
		const int j = get_global_id(0);
		const int i = get_global_id(1);

		if (j >= colonne) return;
		if (i >= righe) return;

		const int value_to_add = i+j;
		m_A[i * colonne + j] = value_to_add;
}

kernel void computesums(const global int* restrict m_A, global int* restrict sum_righe, local int* restrict tmpRighe, global int* restrict sum_cols, 
	local int* restrict tmpCols, global int* restrict sum_tot, local int* restrict tmpTot, const int righe, const int colonne, const int _lws){
		const int j = get_global_id(0);
		const int i = get_global_id(1);
		const int k = get_local_id(0);
		const int w = get_local_id(1);

		if (j >= colonne) return;
		if (i >= righe) return;

		if(w == 0){
			tmpRighe[k] = 0;
			tmpCols[k] = 0;
			tmpTot[k] = 0;
		} 
		
		const int value_to_add = m_A[i*colonne + j];
		barrier(CLK_LOCAL_MEM_FENCE);
		atomic_add(&tmpRighe[k], value_to_add);
		atomic_add(&tmpCols[w], value_to_add);
		atomic_add(&tmpTot[(get_local_id(0))&(_lws-1)], value_to_add);
		barrier(CLK_LOCAL_MEM_FENCE);

		if(w == get_local_size(1)-1)	atomic_add(&sum_cols[j], tmpRighe[k]);
		if(k == get_local_size(0)-1)	atomic_add(&sum_righe[i], tmpCols[w]);
		if(w == get_local_size(1)-1 && k == get_local_size(0)-1){
			int accum = 0;
			for(int p = 0; p < _lws; p++){
				accum += tmpTot[p];
			}
			atomic_add(&sum_tot[0], accum);
		}
	}

kernel void reinit (global float* restrict m_attesi, const global int* restrict m_A, const global int* restrict sum_righe, 
	const global int* restrict sum_cols, const global int* restrict sum_tot, const int righe, const int colonne){
	const int j = get_global_id(0);
	const int i = get_global_id(1);

	if (j >= colonne) return;
	if (i >= righe) return;

	const long riga = (long)sum_righe[i];
	const long colonna = (long)sum_cols[j];
	const float somma = (float)sum_tot[0];
	const int val_A = m_A[i * colonne + j];
	const float tmp_att = riga*colonna/somma;
	m_attesi[i * colonne + j] = ((val_A - tmp_att)*(val_A - tmp_att))/(float)tmp_att;
}

kernel void reduce_chi (global float8* restrict m_attesi, global float* restrict chi_accum,
		int nels){
	const int i = get_global_id(0);
	const int gws = get_global_size(0);

	if (i >= nels) return;
	
	float8 val_0 = m_attesi[i+0*gws];
	float8 val_1 = m_attesi[i+1*gws];
	float8 val_2 = m_attesi[i+2*gws];
	float8 val_3 = m_attesi[i+3*gws];
	float8 val_4 = m_attesi[i+4*gws];
	float8 val_5 = m_attesi[i+5*gws];
	float8 val_6 = m_attesi[i+6*gws];
	float8 val_7 = m_attesi[i+7*gws];

	float8 s0 = (val_0 + val_1) + (val_2 + val_3);
	float8 s1 = (val_4 + val_5) + (val_6 + val_7);
	float8 s2 = s0 + s1; 

	chi_accum[i] = s2.s0 + s2.s1 + s2.s2 + s2.s3 + s2.s4 + s2.s5 + s2.s6 + s2.s7;
}

kernel void reduce_chi_lmem (global const float8* restrict m_attesi, global float* restrict chi_accum,
		local float8* mem_acc, const int lws, const int nels){
	int i = get_global_id(0);
	const int gws = get_global_size(0);
	const int j = get_local_id(0);
	float8 v = 0;

	while (i < nels){
		v += m_attesi[i];
		i+= gws;
	}

	mem_acc[j] = v;

	if(j == get_local_size(0)-1){
		float8 v_2 = (float8)0;
		for(int p = 0; p < lws; p++){
			v_2 += mem_acc[p];
		}
		chi_accum[get_group_id(0)] = (v_2.s0 + v_2.s1) + (v_2.s2 + v_2.s3) + (v_2.s4 + v_2.s5) + (v_2.s6 + v_2.s7);
	}
}


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void main(int argc, char* argv[]) {
	double average_exec_time = 0;

	if (argc < 3) {
		fprintf(stderr, "Inserisci numero righe e colonne");
		exit(1);
	}

	const int num_righe = atoi(argv[1]);
	const int num_cols = atoi(argv[2]);

	for (int loop = 0; loop < 25; loop++) {
		const double begin = clock();
		int* m_val = malloc(sizeof(int) * num_cols * num_righe);

		for (int i = 0; i < num_righe; i++) {
			for (int j = 0; j < num_cols; j++) {
				m_val[i * num_cols + j] = i + j;
			}
		}

		int* tot_righe = (int*)malloc(sizeof(int) * num_righe);
		int* tot_cols = (int*)malloc(sizeof(int) * num_cols);
		int sum = 0;

		for (int i = 0; i < num_righe; i++) {
			tot_righe[i] = 0;
		}

		for (int i = 0; i < num_cols; i++) {
			tot_cols[i] = 0;
		}

		for (int i = 0; i < num_righe; i++) {
			for (int j = 0; j < num_cols; j++) {
				tot_righe[i] += m_val[i * num_cols + j];
				tot_cols[j] += m_val[i * num_cols + j];
				sum += m_val[i * num_cols + j];
			}
		}

		float* m_att = (float*)malloc(sizeof(float) * num_cols * num_righe);

		for (int i = 0; i < num_righe; i++) {
			for (int j = 0; j < num_cols; j++) {
				m_att[i * num_cols + j] = ((_int64)tot_righe[i] * (_int64)tot_cols[j]) * (1.0 / sum);
			}
		}

		float chi_quadro = 0;

		for (int i = 0; i < num_righe; i++) {
			for (int j = 0; j < num_cols; j++) {
				chi_quadro += (((m_val[i * num_cols + j] - m_att[i * num_cols + j]) *
					(m_val[i * num_cols + j] - m_att[i * num_cols + j])) *
					(1 / m_att[i * num_cols + j]));
			}
		}

		free(m_att);
		free(tot_righe);
		free(tot_cols);
		free(m_val);
		double end = clock();
		average_exec_time += (end - begin) / CLOCKS_PER_SEC;
	}
	printf("Execution time: %.3f msec\n ", average_exec_time*1000.0/25.0);
}
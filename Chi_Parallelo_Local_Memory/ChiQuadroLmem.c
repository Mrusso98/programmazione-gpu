#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#define CL_TARGET_OPENCL_VERSION 120
#include "ocl_boiler.h"

size_t gws_align_init;
size_t gws_align_reinit;
size_t gws_align_chi;

cl_event matinit(cl_kernel matinit_k, cl_command_queue que, cl_mem m_A, cl_int num_righe, cl_int num_cols, cl_int _lws) {
	const int gws[] = { round_mul_up(num_cols, gws_align_init), round_mul_up(num_righe, gws_align_init) };
	const int lws[] = { _lws, _lws };
	cl_int err;
	cl_event matinit_evt;
	cl_uint i = 0;
	err = clSetKernelArg(matinit_k, i++, sizeof(m_A), &m_A);
	ocl_check(err, "set parameter matinit failed", i - 1);
	err = clSetKernelArg(matinit_k, i++, sizeof(num_righe), &num_righe);
	ocl_check(err, "set parameter matinit failed", i - 1);
	err = clSetKernelArg(matinit_k, i++, sizeof(num_cols), &num_cols);
	ocl_check(err, "set parameter matinit failed", i - 1);

	err = clEnqueueNDRangeKernel(que, matinit_k, 2, NULL, gws, lws, 0, NULL, &matinit_evt);
	ocl_check(err, "enqueue matinit failed");
	return matinit_evt;
}

cl_event computesums(cl_kernel computesums_k, cl_command_queue que, cl_mem m_A, cl_mem sum_righe,
	cl_mem sum_cols, cl_mem sum_tot, cl_int num_righe, cl_int num_cols, cl_int _lws, cl_event wait) {
	const int gws[] = { round_mul_up(num_cols, gws_align_init), round_mul_up(num_righe, gws_align_init) };
	const int lws[] = { _lws, _lws };
	cl_int err;
	cl_event computesums_evt;
	cl_uint i = 0;
	err = clSetKernelArg(computesums_k, i++, sizeof(m_A), &m_A);
	ocl_check(err, "set parameter computesums failed", i - 1);
	err = clSetKernelArg(computesums_k, i++, sizeof(sum_righe), &sum_righe);
	ocl_check(err, "set parameter computesums failed", i - 1);
	err = clSetKernelArg(computesums_k, i++, lws[0] * sizeof(cl_int), NULL);
	ocl_check(err, "set parameter computesums failed", i - 1);
	err = clSetKernelArg(computesums_k, i++, sizeof(sum_cols), &sum_cols);
	ocl_check(err, "set parameter computesums failed", i - 1);
	err = clSetKernelArg(computesums_k, i++, lws[0] * sizeof(cl_int), NULL);
	ocl_check(err, "set parameter computesums failed", i - 1);
	err = clSetKernelArg(computesums_k, i++, sizeof(sum_tot), &sum_tot);
	ocl_check(err, "set parameter computesums failed", i - 1);
	err = clSetKernelArg(computesums_k, i++, lws[0] * sizeof(cl_int), NULL);
	ocl_check(err, "set parameter computesums failed", i - 1);
	err = clSetKernelArg(computesums_k, i++, sizeof(num_righe), &num_righe);
	ocl_check(err, "set parameter computesums failed", i - 1);
	err = clSetKernelArg(computesums_k, i++, sizeof(num_cols), &num_cols);
	ocl_check(err, "set parameter computesums failed", i - 1);
	err = clSetKernelArg(computesums_k, i++, sizeof(lws[0]), &lws[0]);
	ocl_check(err, "set parameter computesums failed", i - 1);

	err = clEnqueueNDRangeKernel(que, computesums_k, 2, NULL, gws, lws, 1, &wait, &computesums_evt);
	ocl_check(err, "enqueue computesums failed");
	return computesums_evt;
}
cl_event reinit(cl_kernel reinit_k, cl_command_queue que, cl_mem m_attesi, cl_mem sum_righe, cl_mem sum_cols,
	cl_mem sum_tot, cl_int num_righe, cl_int num_cols, cl_event wait) {

	const int gws[] = { round_mul_up(num_cols, gws_align_reinit), round_mul_up(num_righe, gws_align_reinit) };
	cl_int err;
	cl_event reinit_evt;
	cl_uint i = 0;
	err = clSetKernelArg(reinit_k, i++, sizeof(m_attesi), &m_attesi);
	ocl_check(err, "set parameter reinit failed", i - 1);
	err = clSetKernelArg(reinit_k, i++, sizeof(sum_righe), &sum_righe);
	ocl_check(err, "set parameter reinit failed", i - 1);
	err = clSetKernelArg(reinit_k, i++, sizeof(sum_cols), &sum_cols);
	ocl_check(err, "set parameter reinit failed", i - 1);
	err = clSetKernelArg(reinit_k, i++, sizeof(sum_tot), &sum_tot);
	ocl_check(err, "set parameter reinit failed", i - 1);
	err = clSetKernelArg(reinit_k, i++, sizeof(num_righe), &num_righe);
	ocl_check(err, "set parameter reinit failed", i - 1);
	err = clSetKernelArg(reinit_k, i++, sizeof(num_cols), &num_cols);
	ocl_check(err, "set parameter reinit failed", i - 1);

	err = clEnqueueNDRangeKernel(que, reinit_k, 2, NULL, gws, NULL, 1, &wait, &reinit_evt);
	ocl_check(err, "enqueue reinit failed");
	return reinit_evt;
}

cl_event compute_chi(cl_kernel compute_chi_k, cl_command_queue que, cl_mem m_A,
	cl_mem m_attesi, cl_mem chi_accum, cl_int num_righe, cl_int num_cols, cl_int _lws, cl_event wait) {
	const int gws[] = { round_mul_up(num_cols, gws_align_chi), round_mul_up(num_righe, gws_align_chi) };
	const int lws[] = { _lws, _lws };
	cl_int err;
	cl_event compute_chi_evt;
	cl_uint i = 0;
	err = clSetKernelArg(compute_chi_k, i++, sizeof(m_attesi), &m_attesi);
	ocl_check(err, "set parameter compute_chi failed", i - 1);
	err = clSetKernelArg(compute_chi_k, i++, sizeof(m_A), &m_A);
	ocl_check(err, "set parameter compute_chi failed", i - 1);
	err = clSetKernelArg(compute_chi_k, i++, sizeof(chi_accum), &chi_accum);
	ocl_check(err, "set parameter compute_chi failed", i - 1);
	err = clSetKernelArg(compute_chi_k, i++, _lws*sizeof(float), NULL);
	ocl_check(err, "set parameter compute_chi failed", i - 1);
	err = clSetKernelArg(compute_chi_k, i++, sizeof(lws[0]), &lws[0]);
	ocl_check(err, "set parameter compute_chi failed", i - 1);
	err = clSetKernelArg(compute_chi_k, i++, sizeof(num_righe), &num_righe);
	ocl_check(err, "set parameter compute_chi failed", i - 1);
	err = clSetKernelArg(compute_chi_k, i++, sizeof(num_cols), &num_cols);
	ocl_check(err, "set parameter compute_chi failed", i - 1);

	err = clEnqueueNDRangeKernel(que, compute_chi_k, 2, NULL, gws, lws, 1, &wait, &compute_chi_evt);
	ocl_check(err, "enqueue compute_chi failed");
	return compute_chi_evt;
}

/*Inizializza buffer host*/
void h_init(int* h_A, int* h_sum_righe, int* h_sum_cols, int num_righe, int num_cols) {
	for (int i = 0; i < num_righe; i++)
		h_sum_righe[i] = 0;

	for (int i = 0; i < num_cols; i++)
		h_sum_cols[i] = 0;

	for (int i = 0; i < num_righe; i++) {
		for (int j = 0; j < num_cols; j++) {
			h_A[i * num_cols + j] = i + j;
		}
	}
}

/*Calcola somme su host*/
void h_sum(int* h_A, int* h_sum_righe, int* h_sum_cols, int num_righe, int num_cols, int* h_sum_tot) {
	for (int i = 0; i < num_righe; i++) {
		for (int j = 0; j < num_cols; j++) {
			h_sum_righe[i] += h_A[i * num_cols + j];
			h_sum_cols[j] += h_A[i * num_cols + j];
			*h_sum_tot += h_A[i * num_cols + j];
		}
	}
}

void h_reinit(float* h_attesi, int* map_righe, int* map_cols,
	int map_tot, int num_righe, int num_cols) {
	for (int i = 0; i < num_righe; i++) {
		for (int j = 0; j < num_cols; j++) {
			float value = ((_int64)map_righe[i] * (_int64)map_cols[j]) / (float)map_tot;
			h_attesi[i * num_cols + j] = value;
		}
	}
}

void h_compute_chi(float* m_attesi, int* m_A, int num_righe, int num_cols, float* chi_accum) {
	for (int i = 0; i < num_righe; i++) {
		for (int j = 0; j < num_cols; j++) {
			*chi_accum += (((m_A[i * num_cols + j] - m_attesi[i * num_cols + j]) *
				(m_A[i * num_cols + j] - m_attesi[i * num_cols + j])) /
				(float)(m_attesi[i * num_cols + j]));
		}
	}
}

void main(int argc, char* argv[]) {
	/*Controlla se il numero di parametri � corretto: sono necessari numeri di righe e colonne*/
	if (argc < 4) {
		fprintf(stderr, "inserisci numero di righe e colonne e le dimensioni del lws\n");
		exit(1);
	}


	const int num_righe = atoi(argv[1]); //crea variabile che contiene numero righe
	const int num_cols = atoi(argv[2]); //crea variabile che contiene numero colonne
	const int _lws = atoi(argv[3]);
	const int memsize = num_righe * num_cols * sizeof(int); //crea variabile che rappresenta la memoria occupata dalla matrice principale

	/*Boilerplate OCL*/
	cl_platform_id p = select_platform();
	cl_device_id d = select_device(p);
	cl_context ctx = create_context(p, d);
	cl_command_queue que = create_queue(ctx, d);
	cl_program prog = create_program("ChiQuadroLmem.ocl", ctx, d);

	cl_int err;

	/*Inizializza i kernel del codice device*/
	cl_kernel matinit_k = clCreateKernel(prog, "matinit", &err);
	ocl_check(err, "errore nella creazione del kernel matinit");

	cl_kernel computesums_k = clCreateKernel(prog, "computesums", &err);
	ocl_check(err, "errore nella creazione del kernel computesums");

	cl_kernel reinit_k = clCreateKernel(prog, "reinit", &err);
	ocl_check(err, "errore nella creazione del kernel reinit");

	cl_kernel compute_chi_k = clCreateKernel(prog, "compute_chi", &err);
	ocl_check(err, "errore nella creazione del kernel compute_chi");

	/*Query sul device per ottenere i preferred work group size multiple di ogni kernel*/
	err = clGetKernelWorkGroupInfo(matinit_k, d, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(gws_align_init), &gws_align_init, NULL);
	ocl_check(err, "errore nel calcolo del preferred work group size multiple");
	err = clGetKernelWorkGroupInfo(computesums_k, d, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(gws_align_init), &gws_align_init, NULL);
	ocl_check(err, "errore nel calcolo del preferred work group size multiple");
	err = clGetKernelWorkGroupInfo(reinit_k, d, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(gws_align_reinit), &gws_align_reinit, NULL);
	ocl_check(err, "errore nel calcolo del preferred work group size multiple");
	err = clGetKernelWorkGroupInfo(compute_chi_k, d, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(gws_align_chi), &gws_align_chi, NULL);
	ocl_check(err, "errore nel calcolo del preferred work group size multiple");

	/*Crea buffer device per la computazione e buffer host per la verifica seriale*/
	cl_mem m_A = NULL, sum_righe = NULL, sum_cols = NULL, sum_tot = NULL, m_attesi = NULL, chi_accum = NULL;
	int* h_A = NULL, * h_sum_righe = NULL, * h_sum_cols = NULL;
	float* h_attesi = NULL;
	int h_sum_tot = 0;
	float h_chi_accum = 0;

	/*Inizializza buffer device*/
	m_A = clCreateBuffer(ctx, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY, memsize, NULL, &err);
	ocl_check(err, "errore nella creazione del buffer m_A");
	sum_righe = clCreateBuffer(ctx, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY, num_righe * sizeof(cl_int), NULL, &err);
	ocl_check(err, "errore nella creazione del buffer sum_righe");
	sum_cols = clCreateBuffer(ctx, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY, num_cols * sizeof(cl_int), NULL, &err);
	ocl_check(err, "errore nella creazione del buffer sum_cols");
	sum_tot = clCreateBuffer(ctx, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY, sizeof(cl_int), NULL, &err);
	ocl_check(err, "errore nella creazione del buffer sum_tot");

	/*Inizializza buffer host*/
	h_A = (int*)malloc(memsize);
	h_sum_righe = (int*)malloc(sizeof(int) * num_righe);
	h_sum_cols = (int*)malloc(sizeof(int) * num_cols);

	/*Calcola Chi su host*/
	h_init(h_A, h_sum_righe, h_sum_cols, num_righe, num_cols);

	h_sum(h_A, h_sum_righe, h_sum_cols, num_righe, num_cols, &h_sum_tot);

	h_attesi = (float*)malloc(num_righe * num_cols * sizeof(float));

	h_reinit(h_attesi, h_sum_righe, h_sum_cols, h_sum_tot, num_righe, num_cols);

	h_compute_chi(h_attesi, h_A, num_righe, num_cols, &h_chi_accum);

	/*Crea eventi OpenCL*/
	cl_event matinit_evt, computesums_evt, reinit_evt, chi_evt, map_chi_evt;

	/*Inizializza matrice host e device*/
	matinit_evt = matinit(matinit_k, que, m_A, num_righe, num_cols, _lws);

	computesums_evt = computesums(computesums_k, que, m_A, sum_righe, sum_cols, sum_tot, num_righe, num_cols, _lws, matinit_evt);

	/*Crea buffer valori aspettati, device ed host*/
	m_attesi = clCreateBuffer(ctx, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY, num_righe * num_cols * sizeof(float), NULL, &err);
	ocl_check(err, "errore nella creazione del buffer m_attesi");

	/*Inizializza buffer con i valori aspettati*/
	reinit_evt = reinit(reinit_k, que, m_attesi, sum_righe, sum_cols, sum_tot, num_righe, num_cols, computesums_evt);

	chi_accum = clCreateBuffer(ctx, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY, sizeof(cl_float), NULL, &err);
	chi_evt = compute_chi(compute_chi_k, que, m_A, m_attesi, chi_accum, num_righe, num_cols, _lws, reinit_evt);

	cl_float* chi_quadro = clEnqueueMapBuffer(que, chi_accum, CL_FALSE, CL_MAP_READ,
		0, sizeof(cl_float), 1, &chi_evt, &map_chi_evt, &err);
	ocl_check(err, "errore nella mappatura del buffer chi");

	clWaitForEvents(1, &map_chi_evt);
	printf("Chi � %f\n", chi_quadro[0]);
	printf("Chi � %f\n", h_chi_accum);

	/*Calcola e stampa banda passante e runtimes*/
	const double runtime_init = runtime_ms(matinit_evt);
	const double runtime_sum = runtime_ms(computesums_evt);
	const double runtime_reinit = runtime_ms(reinit_evt);
	const double runtime_chi = runtime_ms(chi_evt);

	const double init_bw = (1.0 * memsize)/ 1.0e6 / runtime_init;
	const double sum_bw = ((1.0 * memsize) + (_lws * sizeof(int) * 2) + sizeof(int)) / 1.0e6 / runtime_sum;
	const double reinit_bw = ((2.0*num_cols*num_righe*sizeof(float))) / 1.0e6 / runtime_reinit;
	const double chi_bw = ((2.0 * num_cols*num_righe*sizeof(float))+((num_cols*num_righe)/(_lws*_lws)*sizeof(float))) / 1.0e6 / runtime_chi;

	printf("init: %dx%d els in %g ms %g GB/s %g GE/s\n",
		num_righe, num_cols, runtime_init, init_bw, num_righe * num_cols / 1.0e6 / runtime_init);
	printf("sum: %dx%d els in %g ms %g GB/s %g GE/s\n",
		num_righe, num_cols, runtime_sum, sum_bw, num_righe* num_cols / 1.0e6 / runtime_sum);
	printf("reinit: %dx%d els in %g ms %g GB/s %g GE/s\n",
		num_righe, num_cols, runtime_reinit, reinit_bw, num_righe * num_cols / 1.0e6 / runtime_reinit);
	printf("chi: %dx%d els in %g ms %g GB/s %g GE/s\n",
		num_righe, num_cols, runtime_chi, chi_bw, num_righe * num_cols / 1.0e6 / runtime_chi);

	printf("Runtime complessivo: %g ms\n", runtime_init + runtime_reinit + runtime_chi+ runtime_sum);

	err = clEnqueueUnmapMemObject(que, chi_accum, chi_quadro, 0, NULL, NULL);
	ocl_check(err, "unmap buffer chi_accum");

	free(h_A);
	free(h_sum_righe);
	free(h_sum_cols);
	free(h_attesi);
	clReleaseKernel(matinit_k);
	clReleaseKernel(reinit_k);
	clReleaseKernel(compute_chi_k);
	clReleaseProgram(prog);
	clReleaseCommandQueue(que);
	clReleaseContext(ctx);
	clReleaseMemObject(m_A);
	clReleaseMemObject(sum_righe);
	clReleaseMemObject(sum_cols);
	clReleaseMemObject(sum_tot);
	clReleaseMemObject(m_attesi);
	clReleaseMemObject(chi_accum);
	exit(0);
}
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void main(int argc, char* argv[]) {
	double average_exec_time = 0;
	for(int loop = 0; loop < 25; loop++){
		double begin = clock();
		if (argc < 3) {
			fprintf(stderr, "Inserisci numero righe e colonne");
			exit(1);
		}

		const int num_righe = atoi(argv[1]);
		const int num_cols = atoi(argv[2]);

		int** m_val = (int**)malloc(sizeof(int*) * num_righe);

		for (int i = 0; i < num_righe; i++) {
			m_val[i] = (int*)malloc(sizeof(int) * num_cols);
		}

		for (int i = 0; i < num_righe; i++) {
			for (int j = 0; j < num_cols; j++) {
				m_val[i][j] = i + j;
			}
		}

		int* tot_righe = (int*)malloc(sizeof(int) * num_righe);
		int* tot_cols = (int*)malloc(sizeof(int) * num_cols);
		int sum = 0;

		for (int i = 0; i < num_righe; i++) {
			tot_righe[i] = 0;
		}

		for (int i = 0; i < num_cols; i++) {
			tot_cols[i] = 0;
		}

		for (int i = 0; i < num_righe; i++) {
			for (int j = 0; j < num_cols; j++) {
				tot_righe[i] += m_val[i][j];
				tot_cols[j] += m_val[i][j];
				sum += m_val[i][j];
			}
		}

		float** m_att = (float**)malloc(sizeof(float*) * num_righe);

		for (int i = 0; i < num_righe; i++) {
			m_att[i] = (float*)malloc(sizeof(float) * num_cols);
		}

		for (int i = 0; i < num_righe; i++) {
			for (int j = 0; j < num_cols; j++) {
				m_att[i][j] = ((_int64)tot_righe[i] * (_int64)tot_cols[j]) / (float)sum;
			}
		}

		float chi_quadro = 0;

		for (int i = 0; i < num_righe; i++) {
			for (int j = 0; j < num_cols; j++) {
				chi_quadro += (((m_val[i][j] - m_att[i][j]) * (m_val[i][j] - m_att[i][j])) / m_att[i][j]);
			}
		}

		double end = clock();
		average_exec_time += (end - begin) / CLOCKS_PER_SEC;
	}
		printf("Execution time: %f msec\n ", average_exec_time*1000/25.0);
}